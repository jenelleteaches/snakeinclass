package com.example.parrot.snakeinclass;

import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;

import com.example.parrot.snakeinclass.GameEngine;

public class MainActivity extends AppCompatActivity {

    GameEngine snakeEngine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get size of the screen
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        // Initialize the GameEngine object
        // Pass it the screen size
        snakeEngine = new GameEngine(this, size.y, size.x);

        // Make GameEngine the view of the Activity
        setContentView(snakeEngine);
    }

    // Android Lifecycle function
    @Override
    protected void onResume() {
        super.onResume();
        snakeEngine.startGame();
    }

    // Stop the thread in snakeEngine
    @Override
    protected void onPause() {
        super.onPause();
        snakeEngine.pauseGame();
    }
}
