package com.example.parrot.snakeinclass;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;



    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------


    // ----------------------------
    // ## GAME GRID (coordinate system)
    // ----------------------------
    // The size in pixels of a snake segment
    private int blockSize;      // height & width of each cell (in px)
    // The size in segments of the playable area
    private final int NUM_BLOCKS_WIDE = 30;
    private int numBlocksHigh;


    // ----------------------------
    // ## SPRITES
    // ----------------------------

    // Apple - location of apple on grid-coordinate
    private int appleXPosition;
    private int appleYPosition;

    // Snake
    private int snakeLength;        // length of snake

    // Location of each snake body section on grid-coordinate
    private int[] snakeXPositions;
    private int[] snakeYPositions;

    private int snakeDirection;     // what direction is snake moving
    // 0 = up, 1 = right, 2 = down , 3 = left

    // GAME STATS VARIABLE
    int score = 0;


    // create BOUNDARIES around the playfield
    int maxX;
    int maxY;
    int minX;
    int minY;


    public GameEngine(Context context, int h, int w) {
        super(context);


        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenHeight = h;
        this.screenWidth = w;

        // SETUP THE GAME GRID


        // The grid is 40 blocks wide
        // On a Google Pixel phone (1080 x 1920), this means that
        // the width of each grid column is ~27 pixels wide
        this.blockSize = this.screenWidth / this.NUM_BLOCKS_WIDE;
        this.numBlocksHigh = this.screenHeight / this.blockSize;

        this.printScreenInfo();

        // SET PLAYFIELD BOUNDARIES
        this.minX = 1;
        this.minY = 1;
        this.maxX = NUM_BLOCKS_WIDE-1;
        this.maxY = numBlocksHigh-10;

        // ADD YOUR SPRITES
        snakeXPositions = new int[200];
        snakeYPositions = new int[200];
        this.spawnSnake();
        this.spawnApple();

    }


    private void printScreenInfo() {
        String TAG="SNAKE-GAME";
        Log.d(TAG, "Block Size: " + this.blockSize + "px");
        Log.d(TAG, "Blocks High: " + this.numBlocksHigh);
        Log.d(TAG, "Blocks Wide: " + this.NUM_BLOCKS_WIDE);
        Log.d(TAG, "Screen h x w = " + this.screenHeight + "," + this.screenWidth);
    }

    private void spawnSnake() {
        // start the snake in the middle of the screen
        this.snakeLength = 1;
        this.snakeXPositions[0] = NUM_BLOCKS_WIDE / 2;
        this.snakeYPositions[0] = numBlocksHigh / 2;

        // set an initial direction for the snake
        this.snakeDirection = 3;

        // 0 = up, 1 = right, 2 = down , 3 = left
    }
    private void spawnApple() {
        Random random = new Random();

        // make sure apple always spawns inside the playfield
        appleXPosition = random.nextInt(this.maxX) + 1;
        appleYPosition = random.nextInt(this.maxY) + 1;
    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    public void updatePositions() {
        // @TODO: Setup the new positions for sprites

        // --- COLLISION DETECTION

        // check if head is touching a wall
        if (    (snakeXPositions[0] <= minX) ||
                (snakeXPositions[0] >= maxX) ||
                (snakeYPositions[0] <= minY) ||
                (snakeYPositions[0] >= maxY) )
        {
            Log.d("SNAKE-GAME", "++ SNAKE IS TOUCHING WALL");

            // game over!
            score = 0;
            this.spawnSnake();
            this.spawnApple();

        }

        // check if snake (x,y) == apple (x,y)
        if ( (snakeXPositions[0] == appleXPosition)
        && (snakeYPositions[0] == appleYPosition)) {

            this.spawnApple();
            score++;

            // Exercise - write the code to make the body increase by 1

            // 0. increase the length of the snake
            // 1. Decide what position the body should be in
            // 2. Add that position to snakeXPositions[1], snakeYPositions[1]
            // 3. Go to your drawSprites() function
            // 4.   - Draw a new rectangle at the (snakeX[1], snakeY[1]) position

            this.snakeLength++;
        }

        // move the body
        for (int i = snakeLength; i > 0; i--) {
            this.snakeXPositions[i] = this.snakeXPositions[i-1];
            this.snakeYPositions[i] = this.snakeYPositions[i-1];
        }

        // move the head
        if (this.snakeDirection == 0) {
            // he's moving up
            this.snakeYPositions[0] = this.snakeYPositions[0] - 1;
        }
        else if (this.snakeDirection == 1) {
            // he's moving right
            this.snakeXPositions[0] = this.snakeXPositions[0] + 1;
        }
        else if (this.snakeDirection == 2) {
            // he's moving
            this.snakeYPositions[0] = this.snakeYPositions[0] + 1;
        }
        else if (this.snakeDirection == 3) {
            // he's moving left
            this.snakeXPositions[0] = this.snakeXPositions[0] - 1;
        }

    }

    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------

            //@TODO: Draw stuff (canvas, paintbrush, etc)
            this.canvas.drawColor(Color.argb(255,212,255,226));

            //this.canvas.drawColor(Color.argb(255,0,0,0));

            // draw your snake, draw your apple

            paintbrush.setColor(Color.WHITE);


            // draw grid-lines on screen
            paintbrush.setStrokeWidth(7);
            for (int i=0; i < NUM_BLOCKS_WIDE; i++) {
                canvas.drawLine(i*blockSize, 0, i*blockSize, numBlocksHigh*blockSize, paintbrush);
            }

            for (int i=0; i < numBlocksHigh; i++)  {
                canvas.drawLine(0, i*blockSize, NUM_BLOCKS_WIDE*blockSize, i*blockSize,paintbrush);
            }


            // Draw the apple
            paintbrush.setColor(Color.RED);
            this.canvas.drawRect(appleXPosition * blockSize,
                    (appleYPosition * blockSize),
                    (appleXPosition * blockSize) + blockSize,
                    (appleYPosition * blockSize) + blockSize,
                    paintbrush);


            paintbrush.setColor(Color.BLACK);
            for (int i = 0; i < snakeLength;i++) {
                this.canvas.drawRect(
                        (snakeXPositions[i] * blockSize),
                        (snakeYPositions[i] * blockSize),
                        (snakeXPositions[i] * blockSize) + blockSize,
                        (snakeYPositions[i] * blockSize) + blockSize,
                        paintbrush);
            }


            // put the score on the screen
            paintbrush.setTextSize(50);
            this.canvas.drawText("Score: " + score, 50, 300, paintbrush);


            // draw borders of playfield
            paintbrush.setStrokeWidth(5);

            // top
            this.canvas.drawLine(this.minX*blockSize, this.minY*blockSize, this.maxX*blockSize, this.minY*blockSize, paintbrush );
            // bottom
            this.canvas.drawLine(this.minX*blockSize, this.maxY*blockSize, this.maxX*blockSize, this.maxY*blockSize, paintbrush );

            // left wall
            this.canvas.drawLine(this.minX*blockSize,this.minY*blockSize,this.minX*blockSize,this.maxY*blockSize,paintbrush);

            // right wall
            this.canvas.drawLine(this.maxX*blockSize,this.minY*blockSize, this.maxX*blockSize,this.maxY*blockSize,paintbrush);


            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(120);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // every time you tap, turn snake CLOCKWISE (turn snake RIGHT)

        // detect the tap
        // I have no idea what this means.
        // Mandatory nonsense to detect when person taps the screen

        if (event.getAction() == MotionEvent.ACTION_UP) {
            Log.d("SNAKE-GAME", "Person tapped the screen");
            Log.d("SNAKE-GAME", "Snake Direction: " + this.snakeDirection);

            //this.snakeDirection = 3;
            // 0 = up, 1 = right, 2 = down , 3 = left

            // he is facing right
            if (this.snakeDirection ==  1) {
                // change direction to down
                this.snakeDirection = 2;
            }

            // snake is facing down
            else if (this.snakeDirection == 2) {
                // change direction to left
                this.snakeDirection = 3;
            }

            // snake is facing left
            else if (this.snakeDirection == 3) {
                // change direction to go up
                this.snakeDirection = 0;
            }

            // snake is facing up
            else if (this.snakeDirection == 0) {
                // change direction to go right
                this.snakeDirection = 1;
            }

        }

        return true;


    }
}
