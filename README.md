# Snake Game - As built in class

The code for the snake game that we built in class.

* [Expected Output](#expected-output)
* [Key Concepts](#key-concepts)
* [How To Install](#how-to-install)


## Expected output:

![Expected output](documentation/snakeGame.gif)

* black snake 
* red apple
* 4 black lines around the border of the app (represents playfield boundaries)
* white grid lines in the background




## Key Concepts

Things you should learn from this game:

* How to create a custom grid based on a **fixed number** of columns
* How to make a sprite move in 4 directions (up, down, left, rigth)
* How to use an `array` to store all the snake body parts
* How to use a `for-loop` to:
	* move the snake + its body parts
	* dynamically redraw the snake
* How to setup **boundaries** on your playfield


## How to install

1. Start a new Android Project
2. In `MainActivity.java`, delete the default code and replace with this repository's `MainActivity.java` code
3. Add `GameActivity.java` to your project
4. Update the package names in the code to match YOUR project's package names
